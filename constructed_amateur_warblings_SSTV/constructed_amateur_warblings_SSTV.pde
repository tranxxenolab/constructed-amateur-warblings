// COLOR BAND SWITCHING LINES RANDOM COLORS

/*
  Constructed Amateur Warblings

  for MAKE ME A SIGNAL
  STWST 48x7 10.-12.9.2021
  w/ ShuLea Cheang and Franz Xavier
  
*/

// uncomment as desired the "colorSet" below for 
// pre-defined or random sets of colors

/*
  basically want:
  * number of bands (16, 32, or whatever multiple of 256)
  * width of band (some multiple of 320)
  * set of colors, chosen randomly or maybe in some order
  * and then text or something else on top to add gnarly stuff
*/

String CALLSIGN = "OE/KC1EKK";
int w = 320;
int h = 256;
int[] wSegSet = {20, 40, 80, 80, 80, 160, 160, 160};
int wSeg = wSegSet[int(random(wSegSet.length))];
int hSeg = 8;
color[] colorSet1 = {color(0, 0, 0), color(255, 0, 0), color(0, 255, 255), color(0, 0, 255), color(255, 0, 255)};
color[] colorSet2 = {color(0, 0, 0), color(255, 255, 255), color(255, 255, 255), color(0, 0, 0), color(0, 0, 0), color(255, 255, 255), color(0, 0, 0)};
color[] colorSet3 = {color(175, 0, 217), color(220, 0, 223), color(100, 0, 250), color(143, 0, 203), color(189, 0, 235), color(96, 0, 195), color(255, 0, 251)};
color[] colorSet4 = {color(165, 167, 20), color(77, 86, 59), color(42, 106, 105), color(165, 89, 20), color(146, 150, 127)};
color[] colorSet5 = {color(166, 166, 166), color(140, 140, 140)};
color[] colorSet6 = {color(int(random(255)), int(random(255)), int(random(255))),
color(int(random(255)), int(random(255)), int(random(255))),
color(int(random(255)), int(random(255)), int(random(255))),
color(int(random(255)), int(random(255)), int(random(255))),
color(int(random(255)), int(random(255)), int(random(255))),
color(int(random(255)), int(random(255)), int(random(255))),
color(int(random(255)), int(random(255)), int(random(255))),};
color[] colorSet[] = {colorSet1, colorSet2, colorSet3, colorSet4, colorSet5};
//color[] colorSet[] = {colorSet6};
int numColorSets = colorSet.length;
int numLines = 5;
color lineColor = color(40, 245, 100);
int lineWeight = 4;

PFont f;

void setup() {
  size(320, 256);
  noStroke();
  noLoop();
  
  f = createFont("Exo Bold", 20);
  textFont(f);
}

void draw() {
  int colorSetIndex = 0;
  color[] currentColorSet = colorSet[colorSetIndex]; 
  int numColors = currentColorSet.length;
  int colorIndex = 0;
  for (int j = 0; j < (h/hSeg); j++) {

    for (int i = 0; i < (w/wSeg); i++) {
      
      fill(currentColorSet[colorIndex]);
      rect(i*wSeg, j*hSeg, (i+1)*wSeg, (j+1)*hSeg);
      println(i*wSeg, j*hSeg, (i+1)*wSeg, (j+1)*hSeg);
      colorIndex += 1;
      if (colorIndex >= numColors) {
        colorIndex = 0; 
      }
      
      if (i == ((w/wSeg) - 1)) {

      }
    }
    
    colorSetIndex += 1;
      if (colorSetIndex >= numColorSets) {
        colorSetIndex = 0; 
      }
    
    currentColorSet = colorSet[colorSetIndex]; 
    numColors = currentColorSet.length;
    colorIndex = 0; 
    
    wSeg = wSegSet[int(random(wSegSet.length))];
        
  }
  
  for (int j = 0; j < numLines; j++) {
    // Draw a line
    int startingH = hSeg * int(random(h/hSeg));
    stroke(lineColor);
    strokeWeight(lineWeight);
    int currentW = 0;
    int currentH = startingH;
    int oldW = currentW;
    int oldH = currentH;
    for (int i = 0; i < 30; i++) {
      
      currentW += int(random(50));
      currentH = startingH + int(random(hSeg));
      if (currentW > width) {
         currentW = width; 
      }
      line(oldW, oldH, currentW, currentH);  
      
      oldW = currentW;
      oldH = currentH;
    }    
  }
  
  
  
  
  //fill(colorSet[int(random(numColors))]);
  //rect(0, 0, w, h);
  String filename = "color_bands_switching_lines_random_" + year() + nf(month(), 2) + nf(day(), 2) + nf(hour(), 2) + nf(minute(), 2) + nf(second(), 2) + ".jpg";
  fill(color(255, 0, 252));
  textAlign(LEFT);
  text(CALLSIGN + " STWST 48x7 2021", 10, 240);
  println("done");
  save(filename);
}
